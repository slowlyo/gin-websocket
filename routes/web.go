package routes

import (
	"github.com/gin-gonic/gin"
	"slow-chat/app/http/controllers"
)

func InitRoute() {
	r := gin.Default()

	ws := controllers.WebSocketController{}

	r.GET("/ws", ws.Index)

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	_ = r.Run(":8090")
}
