package controllers

import "github.com/gin-gonic/gin"

type BaseController struct {
	gin.HandlerFunc
}
