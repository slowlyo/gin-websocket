## Chat Server For Gin

***
golang 写的 WebSocket 服务, 仅实现了基础的消息转发

#### 技术栈

- golang
- gin

> 根目录的`slow-chat`和`slow-chat.exe`, 分别是`linux`和`windows`的打包文件, 可以直接运行之后访问`ip:8090/ws`即可


客户端请看 [React 仿微信聊天界面](https://gitee.com/slowlyo/react-chat)